import { FETCH_USER_DATA_PENDING, FETCH_USER_DATA_SUCCESSFULLY, FETCH_USES_DATA_ERROR, ON_CLICK_DELETE_BUTTON, ON_CLICK_EDIT_BUTTON } from "../Constants/users.constants"

export const fetchUser = () =>{
    return async(dispatch) =>{
        try{
            var requestOptions = {
                method : 'GET',
                redirect : 'follow'
            }

            await dispatch({
                type : FETCH_USER_DATA_PENDING
            })

            const reponse = await fetch("http://203.171.20.210:8080/crud-api/users/",requestOptions);

            const data = await reponse.json();

            return dispatch({
                type : FETCH_USER_DATA_SUCCESSFULLY,
                payload : data
            })
        }

        catch(error){
            return dispatch({
                type : FETCH_USES_DATA_ERROR
            })
        }
    }
}

export const onClickEdit = (index) =>{
    return {
        type : ON_CLICK_EDIT_BUTTON,
        payload : index
    }
}

export const onClickDelete = (index) =>{
    return {
        type : ON_CLICK_DELETE_BUTTON,
        payload : index
    }
}