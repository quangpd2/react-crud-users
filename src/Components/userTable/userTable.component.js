import { Button, CircularProgress, Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material"
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import { fetchUser, onClickDelete, onClickEdit } from "../../Actions/users.actions";

const UserTable = () =>{
    const dispatch = useDispatch();

    const {pending,dataUser} = useSelector((reduxData) =>{
        return reduxData.userReducer
    })

    useEffect (() =>{
        dispatch(fetchUser());
    },[dispatch]);


    const onClickEditHandle = (index) =>{
        dispatch(onClickEdit(index));
    }

    const onClickDeleteHandle =(index) =>{
        dispatch(onClickDelete(index));
    }
    return(
        <>
            <Grid container mt={4}>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                    <TableContainer>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center">ID</TableCell>
                                    <TableCell align="center">First Name</TableCell>
                                    <TableCell align="center">Last Name</TableCell>
                                    <TableCell align="center">Country</TableCell>
                                    <TableCell align="center">Subject</TableCell>
                                    <TableCell align="center">Customer Type</TableCell>
                                    <TableCell align="center">Register Status</TableCell>
                                    <TableCell align="center">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    pending ?
                                    <TableRow>
                                        <TableCell><CircularProgress></CircularProgress></TableCell>
                                        <TableCell><CircularProgress></CircularProgress></TableCell>
                                        <TableCell><CircularProgress></CircularProgress></TableCell>
                                        <TableCell><CircularProgress></CircularProgress></TableCell>
                                        <TableCell><CircularProgress></CircularProgress></TableCell>
                                        <TableCell><CircularProgress></CircularProgress></TableCell>
                                        <TableCell><CircularProgress></CircularProgress></TableCell>
                                        <TableCell><CircularProgress></CircularProgress></TableCell>
                                        
                                    </TableRow>
                                    :
                                    dataUser.map((element,index) =>{
                                        return <TableRow key={index}>
                                            <TableCell align="center">{element.id}</TableCell>
                                            <TableCell align="center">{element.firstname}</TableCell>
                                            <TableCell align="center">{element.lastname}</TableCell>
                                            <TableCell align="center">{element.country}</TableCell>
                                            <TableCell align="center">{element.subject}</TableCell>
                                            <TableCell align="center">{element.customerType}</TableCell>
                                            <TableCell align="center">{element.registerStatus}</TableCell>
                                            <TableCell align="center">
                                                <Button color="primary"variant="contained" onClick={() => onClickEditHandle(index)} sx={{marginRight : "5px"}}>Sửa</Button>
                                                <Button color="error" variant="contained" onClick={() => onClickDeleteHandle(index)} sx={{marginLeft : "5px"}}>Xóa</Button>
                                            </TableCell>
                                        </TableRow>
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
        </>
    )
}

export default UserTable