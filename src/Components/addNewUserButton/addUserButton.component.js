import { Button,  Grid } from "@mui/material"

const AddUserButton =() =>{
    return(
        <>
            
                <Grid container mt={4} >
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                        <Button color="success" variant="contained">Add User</Button>
                    </Grid>
                </Grid>
            
        </>
    )
}

export default AddUserButton