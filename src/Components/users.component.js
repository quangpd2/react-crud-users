import { Container } from "@mui/material"
import TableTitle from "./userTableTitle/tableTitle.component"
import AddUserButton from "./addNewUserButton/addUserButton.component"
import UserTable from "./userTable/userTable.component"


const User = () =>{
    return(
        <>
            <Container>
                {/* Title For Table */}
                    <TableTitle/>

                {/* Button Add New User */}
                    <AddUserButton/>

                {/* Table  */}
                    <UserTable/>
            </Container>
        </>
    )
}

export default User