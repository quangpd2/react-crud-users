import { Grid, Typography } from "@mui/material"

const TableTitle = () =>{
    return(
        <>
            <Grid container mt={3} textAlign={"center"}>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                    <Typography fontFamily={"monospace"} color={"Highlight"} variant="h3">Danh sách người dùng đăng ký </Typography>
                </Grid>
            </Grid>
        </>
    )
}

export default TableTitle