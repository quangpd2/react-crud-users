import { userReducer } from "./users.reducers";
const { combineReducers } = require("redux");

const rootReducer = combineReducers({
    userReducer
})

export default rootReducer