import { FETCH_USER_DATA_PENDING, FETCH_USER_DATA_SUCCESSFULLY, FETCH_USES_DATA_ERROR, ON_CLICK_DELETE_BUTTON, ON_CLICK_EDIT_BUTTON } from "../Constants/users.constants";

const initialState = {
    pending : false,
    dataUser  : [],
    idUser : ""

}

export const userReducer = (state = initialState,action) =>{
    console.log(action);
    switch (action.type) {
        case FETCH_USER_DATA_PENDING:
            state.pending = true;
            break;
        case FETCH_USER_DATA_SUCCESSFULLY :
            state.pending = false;
            state.dataUser = action.payload;
            break;
        case FETCH_USES_DATA_ERROR :
            break;
        case ON_CLICK_EDIT_BUTTON:
            state.idUser = state.dataUser[action.payload].id;
            console.log(`Nút sửa được click , User Id : ${state.idUser}`);
            break;
        case ON_CLICK_DELETE_BUTTON:
            state.idUser = state.dataUser[action.payload].id;
            
            console.log(`Nút xóa được click , User Id : ${state.idUser}`);
            break;
        default:
            break;
    }
    return{...state}
}

